from setuptools import {% if cookiecutter.package == 'y' %}find_packages, {% endif %}setup

setup(
    name='{{ cookiecutter.package_name }}',
    description='My awesome project',
    url='https://example.com/',
    author='Adam Brenecki',
    author_email='abrenecki@cmv.com.au',
    license='Proprietary',
    setup_requires=["setuptools_scm>=1.11.1"],
    use_scm_version=True,
{% if cookiecutter.package == "y" %}    packages=find_packages(),
    include_package_data=True,
{% else %}    py_modules=['{{ cookiecutter.package_name }}'],
{% endif %}
    install_requires=[
        'begins',
    ],
    extras_require={
        'dev': [
            'pytest',
            'prospector',
        ]
    },
    entry_points={
        'console_scripts': ['{{ cookiecutter.package_name }}={{ cookiecutter.package_name }}{% if cookiecutter.package == 'y' %}.cli{% endif %}:main.start'],
    },
)
