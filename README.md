# cookiecutter-python

A cookiecutter template for Python scripts and packages.

## Features

- Simple CLI argument parsing with [begins](http://begins.readthedocs.io/en/latest/).
- Style checks and static analysis with pre-commit and prospector.
- Tests with pytest and GitLab CI.

## License

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span resource="[_:publisher]" rel="dct:publisher">
    <span property="dct:title">Commercial Motor Vehicles Pty Ltd</span></span>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">cookiecutter-python</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="AU" about="[_:publisher]">
  Australia</span>.
</p>
