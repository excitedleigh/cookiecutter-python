import os
import subprocess

os.rename('_gitignore', '.gitignore')

if "{{ cookiecutter.package }}" != "y":
    os.rename('{{ cookiecutter.package_name }}/cli.py',
              '{{ cookiecutter.package_name }}.py')
    os.rename('{{ cookiecutter.package_name }}/cli_test.py',
              '{{ cookiecutter.package_name }}_test.py')

if "{{ cookiecutter.initial_commit }}" == "y":
    subprocess.check_call(('git', 'init', '.'))
    subprocess.check_call(('pre-commit', 'autoupdate'))
    subprocess.check_call(('git', 'add', '.'))
    subprocess.check_call(('git', 'commit', '-m', 'Initial commit'))
    subprocess.check_call(('pre-commit', 'install'))
