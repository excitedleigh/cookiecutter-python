from contextlib import contextmanager
import shutil
import tempfile

from cookiecutter.main import cookiecutter

@contextmanager
def tempdir():
    path = tempfile.mkdtemp()
    yield path
    shutil.rmtree(path)


def test_defaults():
    with tempdir() as d:
        cookiecutter('.', output_dir=d, no_input=True,
                     extra_context={'initial_commit': 'n'})


def test_single_file():
    with tempdir() as d:
        cookiecutter('.', output_dir=d, no_input=True,
                     extra_context={'package': 'n',
                                    'initial_commit': 'n'})
